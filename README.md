# Deployerbooks: Azure Terraform

[TOC]

There are two operational tools available to use: `deploy`

# Getting Started

The below instructions should get you started on a basic `docker` and `deployment` setup.

You will need to go through a few tutorials to have a working system.

## 1. Deploy Docker Images & Containers

**Select your project's relevant instructions:**

- Airlab Tutorial Week: [`airlab-tutorial/README.md`](airlab-tutorial/docs/docker.md)

This tutorial will setup the following:

- Create the Azure infrastructure (networking, VPN, VMs, etc), for your project.

## 2. Deploy Catkin Workspace

- Airlab Tutorial Week: [`airlab-tutorial/README.md`](airlab-tutorial/docs/catkin.md)

