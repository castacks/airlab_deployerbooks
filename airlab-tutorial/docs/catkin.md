# Catkin Workspaces

## Azure CPU VM

## 1. Restart Docker Containers

Docker shell containers will give the user access to the entire deploy workspace inside a docker container.

- You should be able to do anything inside the docker container that you would do normally do on the host.

**Docker Container Shell Access**

Follow this step, **on the localhost**, not on the Azure remote VM. These steps will create the docker container on the Azure remote VM.

        # go to the deploy top level path
        cd ~/infrastructure_ws/src

        # create all the docker containers
        ./deployer -s azure.cpu1.docker.shell

## 2. Building The Catkin Workspace

Docker shell containers will give the user access to the entire deploy workspace inside a docker container.

- You should be able to do anything inside the docker container that you would do normally do on the host.

**Docker Container Shell Access**

Follow this step, **on the localhost**, not on the Azure remote VM. These steps will create the docker container on the Azure remote VM.

        # go to the deploy top level path
        cd ~/infrastructure_ws/src

        # create all the docker containers
        ./deployer -s azure.cpu1.catkin.build

* * *

## Azure GPU VM

## 1. Restart Docker Containers

Docker shell containers will give the user access to the entire deploy workspace inside a docker container.

- You should be able to do anything inside the docker container that you would do normally do on the host.

**Docker Container Shell Access**

Follow this step, **on the localhost**, not on the Azure remote VM. These steps will create the docker container on the Azure remote VM.

        # go to the deploy top level path
        cd ~/infrastructure_ws/src

        # create all the docker containers
        ./deployer -s azure.gpu1.docker.shell

## 2. Building The Catkin Workspace

Docker shell containers will give the user access to the entire deploy workspace inside a docker container.

- You should be able to do anything inside the docker container that you would do normally do on the host.

**Docker Container Shell Access**

Follow this step, **on the localhost**, not on the Azure remote VM. These steps will create the docker container on the Azure remote VM.

        # go to the deploy top level path
        cd ~/infrastructure_ws/src

        # create all the docker containers
        ./deployer -s azure.gpu1.catkin.build
