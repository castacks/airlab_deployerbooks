# Azure Airlab Docker Setup

## Azure CPU VM

### 1. Building Docker Images

Docker install all the repository dependencies as *docker images*.

- The docker images will be built on the remote Azure VM.

**Docker Image**

Follow this step, **on the localhost**, not on the Azure remote VM. These steps will create the docker image on the Azure remote VM.

        # go to the deploy top level path
        cd ~/infrastructure_ws/src

        # build all the docker images (this can take a long time)
        ./deployer -s azure.cpu1.docker.image

**Access The Remote VM**

        # ssh into your VM (if not already done so), change the below command to match your VM ssh access
        ssh azure.tutorial.cpu

**Cleanup (Required)**

        # Remove any previously created docker containers (optional)
        #   - its okay to ignore error 'Error: No such container' and continue to the next step.
        docker rm -f ros-cpu-shell

        # cleanup dangling docker images
        #   - its okay to ignore error ' "docker rmi" requires at least 1 argument. '
        docker rmi -f $(docker images -f "dangling=true" -q)

**Verify Docker Images**

        # View the docker images built on the remote VM
        docker images

        # verify you see the following docker images (in any order):
        #   ->
        #       airlab-tutorial/cpu:ros

**Return To Localhost**

        # exit the remote VM
        exit

## 2. Creating Docker Containers With Shell Access

Docker shell containers will give the user access to the entire deploy workspace inside a docker container.

- You should be able to do anything inside the docker container that you would do normally do on the host.

**Docker Container Shell Access**

Follow this step, **on the localhost**, not on the Azure remote VM. These steps will create the docker container on the Azure remote VM.

        # go to the deploy top level path
        cd ~/infrastructure_ws/src

        # create all the docker containers
        ./deployer -s azure.cpu1.docker.shell

**Access The Remote VM**

        # ssh into your VM (if not already done so), change the below command to match your VM ssh access
        ssh azure.tutorial.cpu

**Verify Docker Containers**

        # view running docker containers
        docker ps

        # verify you see the following docker containers (in any order):
        #   ->
        #       ros-cpu-shell

**Return To Localhost**

        # exit the remote VM
        exit

* * *

## Azure GPU VM

### 1. Building Docker Images

Docker install all the repository dependencies as *docker images*.

- The docker images will be built on the remote Azure VM.

**Docker Image**

Follow this step, **on the localhost**, not on the Azure remote VM. These steps will create the docker image on the Azure remote VM.

        # go to the deploy top level path
        cd ~/infrastructure_ws/src

        # build all the docker images (this can take a long time)
        ./deployer -s azure.gpu1.docker.image

**Access The Remote VM**

        # ssh into your VM (if not already done so), change the below command to match your VM ssh access
        ssh azure.tutorial.gpu

**Cleanup (Required)**

        # Remove any previously created docker containers (optional)
        #   - its okay to ignore error 'Error: No such container' and continue to the next step.
        docker rm -f ros-gpu-shell

        # cleanup dangling docker images
        #   - its okay to ignore error ' "docker rmi" requires at least 1 argument. '
        docker rmi -f $(docker images -f "dangling=true" -q)

**Verify Docker Images**

        # View the docker images built on the remote VM
        docker images

        # verify you see the following docker images (in any order):
        #   ->
        #       airlab-tutorial/gpu:ros
        #       airlab-tutorial/gpu:opencv

**Return To Localhost**

        # exit the remote VM
        exit

## 2. Creating Docker Containers With Shell Access

Docker shell containers will give the user access to the entire deploy workspace inside a docker container.

- You should be able to do anything inside the docker container that you would do normally do on the host.

**Docker Container Shell Access**

Follow this step, **on the localhost**, not on the Azure remote VM. These steps will create the docker container on the Azure remote VM.

        # go to the deploy top level path
        cd ~/infrastructure_ws/src

        # create all the docker containers
        ./deployer -s azure.gpu1.docker.shell

**Access The Remote VM**

        # ssh into your VM (if not already done so), change the below command to match your VM ssh access
        ssh azure.tutorial.gpu

**Verify Docker Containers**

        # view running docker containers
        docker ps

        # verify you see the following docker containers (in any order):
        #   ->
        #       ros-gpu-shell
        #       opencv-gpu-shell

**Return To Localhost**

        # exit the remote VM
        exit
